package com.rmit.model.database;

//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.ResultSet;
//import java.sql.Statement;
//
//import org.apache.log4j.Logger;

import com.rmit.model.beans.Annotation;
import com.rmit.model.beans.DataBindingUtils;
import com.rmit.model.beans.User;
import com.rmit.service.stubs.AnnotationServiceStub;

public class ApplicationDatabase {
	// private final Logger logger = Logger.getLogger("dbConnection");
	// private String driver = "org.apache.derby.jdbc.ClientDriver";
	// private String protocol = "jdbc:derby://localhost:1527/";
	// private Connection con;
	// private Statement st;
	private AnnotationServiceStub stub;

	public ApplicationDatabase() {
	}

	// Method to log in as a valid user
	public User loginUser(String username, String password) {
		User user = null;
		try {
			stub = new AnnotationServiceStub(
					"http://localhost:8080/axis2/services/AnnotationService");
			AnnotationServiceStub.User stubUser = stub
					.login(username, password);

			if (stubUser != null) {
				user = DataBindingUtils.toBeanUser(stubUser);
			}
			// Garbage collect stubUser, stub
			stubUser = null;
			stub = null;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return user;
	}

	// Method to create an annotation and send to database through stub
	public String makeAnnotation(String userID, String annotationContent,
			String docID, int pos1, int pos2) {
		try {
			stub = new AnnotationServiceStub(
					"http://localhost:8080/axis2/services/AnnotationService");
			String id = stub.createAnnotation(userID, docID, annotationContent,
					pos1, pos2);

			stub = null;

			return id;

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	// Method to retrieve annotation from database through stub
	public Annotation[] retrieveAnnotations(String docID) {
		Annotation[] annotations = null;
		try {
			stub = new AnnotationServiceStub(
					"http://localhost:8080/axis2/services/AnnotationService");
			AnnotationServiceStub.Annotation[] stubAnnotations = stub
					.retrieveAnnotations(docID);

			if (stubAnnotations != null) {
				annotations = new Annotation[stubAnnotations.length];

				for (int i = 0; i < stubAnnotations.length; i++) {
					annotations[i] = DataBindingUtils
							.toBeanAnnotation(stubAnnotations[i]);
				}
			}
			stub = null;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return annotations;
	}

	// Method to retrieve users from database through stub
	public User[] retrieveUsers() {
		User[] users = null;
		try {
			stub = new AnnotationServiceStub(
					"http://localhost:8080/axis2/services/AnnotationService");
			AnnotationServiceStub.User[] stubUsers = stub.getUsers();

			if (stubUsers != null) {
				users = new User[stubUsers.length];

				for (int i = 0; i < stubUsers.length; i++) {
					users[i] = DataBindingUtils.toBeanUser(stubUsers[i]);
				}
			}
			stub = null;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return users;
	}
	
	// Method to retrieve a single annotation based on its ID
	public Annotation retrieveAnnotationById(String annotationId){
		Annotation annotation = null;
		try { 
			stub = new AnnotationServiceStub(
					"http://localhost:8080/axis2/services/AnnotationService");
			AnnotationServiceStub.Annotation stubAnnotation = stub.retrieveAnnotationById(annotationId);
			if(stubAnnotation != null){
				annotation = DataBindingUtils.toBeanAnnotation(stubAnnotation);
			}
			stub = null;
			stubAnnotation = null;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return annotation;
	}
}
