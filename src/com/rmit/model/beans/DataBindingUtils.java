package com.rmit.model.beans;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.beanutils.BeanUtils;

import com.rmit.service.stubs.AnnotationServiceStub;


// Convert between the various paramater types used by adb framework
// makes sense since these would all be from different business and we expect to convert
public class DataBindingUtils {
	
	// Customer Conversions
	public static com.rmit.model.beans.User toBeanUser(
			AnnotationServiceStub.User serviceUser) {
		com.rmit.model.beans.User user = new com.rmit.model.beans.User();

		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(user, serviceUser);
		} catch (Exception e) {
			logError(e);
		}

		assert user != null : "data binding error with User bean";
		return user;
	}

	public static AnnotationServiceStub.User toAnnotationUser(
			com.rmit.model.beans.User user) {
		AnnotationServiceStub.User serviceUser = new AnnotationServiceStub.User();

		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(serviceUser, user);
		} catch (Exception e) {
			logError(e);
		}

		assert serviceUser != null : "data binding error with User bean";
		return serviceUser;
	}

//	public static com.rmit.model.beans.Document toAnnotationsDocument(
//			AnnotationServiceStub.Document serviceDocument) {
//		com.rmit.model.beans.Document document = new com.rmit.model.beans.Document();
//
//		try {
//			// uses commons-beanutils-xxx.jar in lib/
//			BeanUtils.copyProperties(document, serviceDocument);
//		} catch (Exception e) {
//			logError(e);
//		}
//
//		assert document != null : "data binding error with Document bean";
//		return document;
//	}

//	public static AnnotationServiceStub.Document toBeanDocument(
//			model.annotations.Document document) {
//		AnnotationServiceStub.Document serviceDocument = new AnnotationServiceStub.Document();
//
//		try {
//			// uses commons-beanutils-xxx.jar in lib/
//			BeanUtils.copyProperties(serviceDocument, document);
//		} catch (Exception e) {
//			logError(e);
//		}
//
//		assert serviceDocument != null : "data binding error with Document bean";
//		return serviceDocument;
//	}
//
	public static com.rmit.model.beans.Annotation toBeanAnnotation(
			AnnotationServiceStub.Annotation serviceAnnotation) {
		com.rmit.model.beans.Annotation annotation = new com.rmit.model.beans.Annotation();

		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(annotation, serviceAnnotation);
		} catch (Exception e) {
			logError(e);
		}

		assert annotation != null : "data binding error with Annotation bean";
		return annotation;
	}

	public static AnnotationServiceStub.Annotation toAnnotationAnnotation(
			com.rmit.model.beans.Annotation annotation) {
		AnnotationServiceStub.Annotation serviceAnnotation = new AnnotationServiceStub.Annotation();

		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(serviceAnnotation, annotation);
		} catch (Exception e) {
			logError(e);
		}

		assert serviceAnnotation != null : "data binding error with Annotation bean";
		return serviceAnnotation;
	}

	public static void logError(Exception e) {
		// Use built in Java logger
		Logger logger = Logger.getLogger("assignment1");

		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();

		logger.log(Level.SEVERE, exceptionAsString);
	}
}
