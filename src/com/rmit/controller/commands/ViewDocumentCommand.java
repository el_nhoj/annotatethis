package com.rmit.controller.commands;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.commons.io.FileUtils;

import com.rmit.model.beans.Annotation;
import com.rmit.model.beans.User;
import com.rmit.model.database.ApplicationDatabase;

public class ViewDocumentCommand extends AbstractCommand {

	public ViewDocumentCommand() {
		super();
	}

	@Override
	public void execute() {

		session = req.getSession(false);

		try {
			if (session.getAttribute("userID") != null) {
				ApplicationDatabase appDB = new ApplicationDatabase();
				Annotation[] annotations = null;
				User[] users = null;
				
				String filename = (String) req.getParameter("filename");
				session.setAttribute("currentDocument", filename);
				
				// Check if filename parameter has been passed
				if (filename != null) {

					// Set appropriate absolute path to documents folder
					String folder = "D:/Documents/Java/IIDE-Assignment-02/WebContent/documents/";

					File file = new File(folder + filename);

					// Read contents of text file to string
					String fileContent = FileUtils.readFileToString(file,
							"UTF-8");

					// Retrieve existing annotations
					annotations = appDB.retrieveAnnotations(filename);
					
					// Retrieve existing users
					users = appDB.retrieveUsers();
					
					// If there are annotations, set annotation array to page
					if (annotations != null) {
						req.setAttribute("annotations", annotations);
					}
					
					// If there are users, set users array to page
					if (users != null) {
						req.setAttribute("users", users);
					}

					// Send filename and file content
					req.setAttribute("filename", filename);
					req.setAttribute("fileContent", fileContent);
					req.getRequestDispatcher("viewDocument.jsp").forward(req,
							resp);
				} else {
					req.setAttribute("error", "No document was selected!");
					req.getRequestDispatcher("browseDocuments.jsp").forward(
							req, resp);
				}
			} else {
				req.setAttribute("error", "Please log in.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			}
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}

	}

}
