package com.rmit.controller.commands;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.commons.io.FileUtils;

public class SearchDocumentCommand extends AbstractCommand {
	
	public SearchDocumentCommand() {
		super();
	}

	@Override
	public void execute() {

		session = req.getSession(false);

		try {
			if (session.getAttribute("userID") != null) {
				
				String filename = (String) session.getAttribute("currentDocument");

				// Check if filename parameter has been passed
				if (filename != null) {

					// Set appropriate absolute path to documents folder
					String folder = "D:/Documents/Java/IIDE-Assignment-02/WebContent/documents/";

					File file = new File(folder + filename);

					// Read contents of text file to string
					String fileContent = FileUtils.readFileToString(file,
							"UTF-8");

					// Send filename and file content
					req.setAttribute("filename", filename);
					req.setAttribute("fileContent", fileContent);
					req.getRequestDispatcher("searchDocument.jsp").forward(req,
							resp);
				} else {
					req.setAttribute("error", "No document was selected!");
					req.getRequestDispatcher("browseDocuments.jsp").forward(
							req, resp);
				}
			} else {
				req.setAttribute("error", "Please log in.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			}
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}

	}

}