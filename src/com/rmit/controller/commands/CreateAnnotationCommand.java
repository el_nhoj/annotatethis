package com.rmit.controller.commands;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;

import org.apache.commons.io.FileUtils;

import com.rmit.model.beans.Annotation;
import com.rmit.model.beans.User;
import com.rmit.model.database.ApplicationDatabase;

public class CreateAnnotationCommand extends AbstractCommand {

	public CreateAnnotationCommand() {
		super();
	}

	@Override
	public void execute() {

		session = req.getSession(false);

		String id;
		ApplicationDatabase appDB = new ApplicationDatabase();
		Annotation[] annotations = null;
		User[] users = null;

		try {
			if (session.getAttribute("userID") != null) {
				String userID = req.getParameter("userID");
				String annotationContent = req
						.getParameter("annotationContent");
				String docID = req.getParameter("docID");
				int pos1 = Integer.parseInt(req.getParameter("pos1"));
				int pos2 = Integer.parseInt(req.getParameter("pos2"));

				// Check form fields for user credentials and set session if
				// valid
				id = appDB.makeAnnotation(userID, annotationContent, docID,
						pos1, pos2);
				if (id != null) {
					try {
						if (docID != null) {
							// Set appropriate absolute path to annotations folder
							PrintWriter writer = new PrintWriter(
									"D:/Documents/Java/IIDE-Assignment-02/annotations/"
											+ docID + "/" + id + ".txt",
									"UTF-8");
							writer.println(annotationContent);
							writer.close();

							// Set appropriate absolute path to documents folder
							String folder = "D:/Documents/Java/IIDE-Assignment-02/WebContent/documents/";

							File file = new File(folder + docID);

							// Read contents of text file to string
							String fileContent = FileUtils.readFileToString(
									file, "UTF-8");

							// Retrieve existing annotations
							annotations = appDB.retrieveAnnotations(docID);
							// Retrieve existing users
							users = appDB.retrieveUsers();

							// If there are annotations, set annotation array to
							// page
							if (annotations != null) {
								req.setAttribute("annotations", annotations);
							}
							// If there are users, set users array to page
							if (users != null) {
								req.setAttribute("users", users);
							}

							// Send filename and file content
							req.setAttribute("filename", docID);
							req.setAttribute("fileContent", fileContent);
							req.getRequestDispatcher("viewDocument.jsp")
									.forward(req, resp);
						} else {
							req.setAttribute("error",
									"An error occurred, please try again.");
							req.getRequestDispatcher("browseDocuments.jsp")
									.forward(req, resp);
						}
					} catch (IOException ioe) {
						out.println(ioe);
					} catch (ServletException se) {
						out.println(se);
					}
				} else {
					req.setAttribute("error",
							"The annotation was not created. Please try again!");
					req.setAttribute("filenames", docID);
					req.getRequestDispatcher("browseDocuments.jsp").forward(
							req, resp);
				}
			} else {
				req.setAttribute("error", "Please log in.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			}
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}
	}

}