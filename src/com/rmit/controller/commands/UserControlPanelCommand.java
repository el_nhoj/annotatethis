package com.rmit.controller.commands;

import java.io.IOException;

import javax.servlet.ServletException;

public class UserControlPanelCommand extends AbstractCommand {

	public UserControlPanelCommand() {
		super();
	}

	@Override
	public void execute() {

		session = req.getSession(false);

		try {
			if (session.getAttribute("userID") != null) {
				req.getRequestDispatcher("userControlPanel.jsp").forward(req,
						resp);
			} else {
				req.setAttribute("error", "Please log in.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			}
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}

	}

}
