package com.rmit.controller.commands;

import java.io.IOException;

import javax.servlet.ServletException;

import com.rmit.model.beans.Annotation;
import com.rmit.model.database.ApplicationDatabase;

public class ShareAnnotationCommand extends AbstractCommand {
	
	public ShareAnnotationCommand() {
	}

	@Override
	public void execute() {
		
		ApplicationDatabase appDB = new ApplicationDatabase();
		
		try {
			Annotation annotation = null;
			
			String annotationId = req.getParameter("annotationId");
			
			if(annotationId != null){
				// Attempt to retrieve annotation using annotation ID
				annotation = appDB.retrieveAnnotationById(annotationId);
				
				if(annotation != null){
					req.setAttribute("annotation", annotation);
					req.getRequestDispatcher("shareAnnotation.jsp").forward(
							req, resp); 
				} else {
					req.setAttribute("error", "No annotation was found!");
					req.getRequestDispatcher("shareAnnotation.jsp").forward(
							req, resp); 
				}				
			} else {
				req.setAttribute("error", "No annotation was selected!");
				req.getRequestDispatcher("shareAnnotation.jsp").forward(
						req, resp);
			}
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}
		
	}

}
