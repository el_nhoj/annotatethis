package com.rmit.controller.commands;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;

import lucene.LuceneSearch;

import org.apache.commons.io.FileUtils;

import com.rmit.model.beans.Annotation;
import com.rmit.model.beans.User;
import com.rmit.model.database.ApplicationDatabase;

public class ViewResultsCommand extends AbstractCommand {
	
	public ViewResultsCommand() {
		super();
	}

	@Override
	public void execute() {

		session = req.getSession(false);
		String keywords = (String) req.getParameter("keywords");
		if (keywords == "") {
			try {
				if (session.getAttribute("userID") != null) {

					String filename = (String) session.getAttribute("currentDocument");
					
					// Check if filename parameter has been passed
					if (filename != null) {
	
						// Set appropriate absolute path to documents folder
						String folder = "D:/Documents/Java/IIDE-Assignment-02/WebContent/documents/";
	
						File file = new File(folder + filename);
	
						// Read contents of text file to string
						String fileContent = FileUtils.readFileToString(file,
								"UTF-8");
						
						// Send filename and file content
						req.setAttribute("filename", filename);
						req.setAttribute("fileContent", fileContent);
						req.setAttribute("error", "Please enter some keywords.");
						req.getRequestDispatcher("searchDocument.jsp").forward(req,
								resp);
					} else {
						req.setAttribute("error", "No document was selected!");
						req.getRequestDispatcher("browseDocuments.jsp").forward(
								req, resp);
					}
				} else {
					req.setAttribute("error", "Please log in.");
					req.getRequestDispatcher("login.jsp").forward(req, resp);
				}
			} catch (IOException ioe) {
				out.println(ioe);
			} catch (ServletException se) {
				out.println(se);
			}
		} else {
			try {
				if (session.getAttribute("userID") != null) {
					ApplicationDatabase appDB = new ApplicationDatabase();
					Annotation[] annotations = null;
					User[] users = null;
					
					session = req.getSession(true);
					String filename = (String) session.getAttribute("currentDocument");
					LuceneSearch search = new LuceneSearch();
					String[] results = search.search(keywords, filename);
					Annotation[] annotationResults = new Annotation[results.length];
					
					// Check if filename parameter has been passed
					if (filename != null) {
	
						// Set appropriate absolute path to documents folder
						String folder = "D:/Documents/Java/IIDE-Assignment-02/WebContent/documents/";
	
						File file = new File(folder + filename);
	
						// Read contents of text file to string
						String fileContent = FileUtils.readFileToString(file,
								"UTF-8");
						// Retrieve existing annotations
						annotations = appDB.retrieveAnnotations(filename);
						// Retrieve existing users
						users = appDB.retrieveUsers();
						// get annotations that match the results
						for(Annotation a : annotations){
							for(int i=0; i < results.length; i++){
								if(a.getAnnotationId().equals(results[i])){
									annotationResults[i] = new Annotation();
									annotationResults[i].setAnnotationId(a.getAnnotationId());
									annotationResults[i].setAnnotationContent(a.getAnnotationContent());
									annotationResults[i].setDocId(a.getDocId());
									annotationResults[i].setUserId(a.getUserId());
									annotationResults[i].setStartPosition(a.getStartPosition());
									annotationResults[i].setEndPosition(a.getEndPosition());
								}
							}						
						}
						// If there are annotations, set annotation array to page
						if (annotationResults != null) {
							req.setAttribute("annotations", annotationResults);
						}
						if(results.length == 0){
							req.setAttribute("error", "There are no matching annotations.");
						}
						// If there are users, set users array to page
						if (users != null) {
							req.setAttribute("users", users);
						}
						// Send filename and file content
						req.setAttribute("filename", filename);
						req.setAttribute("fileContent", fileContent);
						req.getRequestDispatcher("searchDocument.jsp").forward(req,
								resp);
					} else {
						req.setAttribute("error", "No document was selected!");
						req.getRequestDispatcher("browseDocuments.jsp").forward(
								req, resp);
					}
				} else {
					req.setAttribute("error", "Please log in.");
					req.getRequestDispatcher("login.jsp").forward(req, resp);
				}
			} catch (IOException ioe) {
				out.println(ioe);
			} catch (ServletException se) {
				out.println(se);
			}
		}

	}

}