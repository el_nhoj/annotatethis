package com.rmit.controller.commands;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

public class BrowseDocumentsCommand extends AbstractCommand {

	public BrowseDocumentsCommand() {
		super();
	}

	@Override
	public void execute() {

		session = req.getSession(false);

		try {
			if (session.getAttribute("userID") != null) {
				// Change to your web project's absolute WebContent/documents
				// path
				File folder = new File(
						"D:/Documents/Java/IIDE-Assignment-02/WebContent/documents");
				File[] fileList = folder.listFiles();
				List<String> filenames = new ArrayList<String>();

				if (fileList != null) {
					// Add filename to list if ends in ".txt"
					for (File f : fileList) {
						if (f.isFile() && f.getName().endsWith(".txt")) {
							filenames.add(f.getName());
							System.out.println(f.getName());
						}
					}

					// Pass filename list to JSP page
					req.setAttribute("filenames", filenames);
					req.getRequestDispatcher("browseDocuments.jsp").forward(
							req, resp);
				} else {
					req.setAttribute("error", "Documents directory is empty.");
					req.getRequestDispatcher("browseDocuments.jsp").forward(
							req, resp);
				}
			} else {
				req.setAttribute("error", "Please log in.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			}
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}
	}

}
