package com.rmit.controller.commands;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Command {
	public void execute();
	public void init(HttpServletRequest req, HttpServletResponse resp);
}
