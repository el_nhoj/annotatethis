package com.rmit.controller.commands;

import java.io.IOException;

import javax.servlet.ServletException;

import com.rmit.model.beans.User;
import com.rmit.model.database.ApplicationDatabase;

public class LoginCommand extends AbstractCommand {

	public LoginCommand() {
		super();
	}

	@Override
	public void execute() {
		User user = null;
		ApplicationDatabase appDB = new ApplicationDatabase();

		try {
			String un = req.getParameter("username");
			String pw = req.getParameter("password");

			// Check form fields for user credentials and set session if
			// valid
			user = appDB.loginUser(un, pw); 
			if (user != null) {

				session = req.getSession(true);

				// Set session variables
				session.setAttribute("userID", user.getName());
				session.setAttribute("user", user);

				req.getRequestDispatcher("userControlPanel.jsp").forward(req, resp);

			} else {
				req.setAttribute("error",
						"Wrong username or password. Please try again.");
				req.getRequestDispatcher("login.jsp").forward(req, resp);
			}
		} catch (IOException ioe) {
			out.println(ioe);
		} catch (ServletException se) {
			out.println(se);
		}
	}

}
