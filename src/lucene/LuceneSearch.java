package lucene;

import java.io.IOException;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.TopDocs;

public class LuceneSearch {

	String indexDir = "index";
	String dataDir;
	Indexer indexer;
	Searcher searcher;

	public String[] search(String keywords, String filename) {
		dataDir = "D:/Documents/Java/IIDE-Assignment-02/annotations/"
				+ filename + "";
		String[] results = null;
		try {
			createIndex();
			results = sortUsingRelevance(keywords);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return results;
	}

	private String[] sortUsingRelevance(String searchQuery) throws IOException,
			ParseException {
		searcher = new Searcher(indexDir);
		long startTime = System.currentTimeMillis();

		QueryParser parser = new QueryParser(LuceneConstants.CONTENTS,
				new StandardAnalyzer());
		// Parse
		Query query = parser.parse(searchQuery);

		searcher.setDefaultFieldSortScoring(true, false);
		// do the search
		TopDocs hits = searcher.search(query, Sort.RELEVANCE);
		long endTime = System.currentTimeMillis();
		String[] fileIDs = new String[hits.totalHits];
		int count = 0;
		System.out.println(hits.totalHits + " documents found. Time :"
				+ (endTime - startTime) + "ms");
		for (ScoreDoc scoreDoc : hits.scoreDocs) {
			Document doc = searcher.getDocument(scoreDoc);
			System.out.print("Score: " + scoreDoc.score + " ");
			System.out.println("File: " + doc.get(LuceneConstants.FILE_NAME));
			fileIDs[count] = doc.get(LuceneConstants.FILE_NAME).substring(0,
					doc.get(LuceneConstants.FILE_NAME).length() - 4);
			count++;
		}
		searcher.close();
		return fileIDs;
	}

	private void sortUsingIndex(String searchQuery) throws IOException,
			ParseException {
		searcher = new Searcher(indexDir);
		long startTime = System.currentTimeMillis();
		// create a term to search file name
		Term term = new Term(LuceneConstants.FILE_NAME, searchQuery);
		// create the term query object
		Query query = new FuzzyQuery(term);
		searcher.setDefaultFieldSortScoring(true, false);
		// do the search
		TopDocs hits = searcher.search(query, Sort.INDEXORDER);
		long endTime = System.currentTimeMillis();
		System.out.println(hits.totalHits + " documents found. Time :"
				+ (endTime - startTime) + "ms");
		for (ScoreDoc scoreDoc : hits.scoreDocs) {
			Document doc = searcher.getDocument(scoreDoc);
			System.out.print("Score: " + scoreDoc.score + " ");
			System.out.println("File: " + doc.get(LuceneConstants.FILE_PATH));
		}
		searcher.close();
	}

	private void createIndex() throws IOException {
		indexer = new Indexer(indexDir);
		int numIndexed;
		long startTime = System.currentTimeMillis();
		numIndexed = indexer.createIndex(dataDir, new TextFileFilter());
		long endTime = System.currentTimeMillis();
		indexer.close();
		System.out.println(numIndexed + " File indexed, time taken: "
				+ (endTime - startTime) + " ms");
	}
}