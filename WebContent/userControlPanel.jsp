<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/includes/header.jsp"%>
<h1>User Control Panel</h1>
<c:if test="${sessionScope.userID != null}">
	<div class="left">
		<h2>
			Welcome back, <span class="bold">${sessionScope.userID}</span>!
		</h2>
	</div>
	<div class="right">
		<form action="getAction" method="post" name="logoutForm">
			<button type="submit" name="command" value="logout">Logout</button>
		</form>
	</div>
</c:if>
<p>
	To begin reading and annotating, please click on the <span class="bold">"Browse
		Documents"</span> button below or in the navigation bar to browse our
	available documents!
</p>
<p>
	<a href="getAction?command=browseDocuments" class="viewButton">Browse
		Documents</a>
</p>
<%@ include file="/includes/footer.html"%>