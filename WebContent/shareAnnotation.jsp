<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Share Annotation - Annotate This!</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link href='https://fonts.googleapis.com/css?family=Lora'
	rel='stylesheet' type='text/css'>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<meta property="og:url"                content="http://localhost:8080<c:url value="/getAction?command=shareAnnotation&annotationId=${annotation.annotationId}" />" />
<meta property="og:type"               content="website" />
<meta property="og:title"              content="Share with Annotate This!" />
<meta property="og:description"        content="${annotation.annotationContent}" />
<meta property="og:image"              content="http://i.imgur.com/eBCp4sq.png" />
</head>
<body>
	<div id="wrapper">
		<header>
			<div id="header-content">
				<p>
					<img src="images/banner.png" alt="Logo" />
				</p>
			</div>
		</header>
		<nav>
			<div id="nav-content">
				<ul>
					<li><a href="getAction?command=userControlPanel">User CP</a></li>
					<li><a href="getAction?command=browseDocuments">Browse
							Documents</a></li>
					<c:choose>
						<c:when test="${sessionScope.userID != null}">
							<li><a href="getAction?command=logout">Logout</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="/IIDE-Assignment-02">Login</a></li>
						</c:otherwise>
					</c:choose>
				</ul>
			</div>
		</nav>
		<div id="main">
			<h1>Share This Annotation</h1>
			<p class="error">${error}</p>
			<p>
				<a href="getAction?command=browseDocuments" class="viewButton">Return
					to Browsing</a>
			</p>
			<c:if test="${annotation != null}">
				<div id="singleAnnotation">
					<h1>You wrote: </h1>
					<p><c:out value="${annotation.annotationContent}" /></p>		
					
  						<p class="bold">Full URL:</p> 
  						<p class="linkURL">http://localhost:8080<c:url value="/getAction?command=shareAnnotation&annotationId=${annotation.annotationId}" /></p>			
				<script>
					window.fbAsyncInit = function() {
						FB.init({
							appId : '889742367783030',
							xfbml : true,
							version : 'v2.5'
						});
					};

					(function(d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) {
							return;
						}
						js = d.createElement(s);
						js.id = id;
						js.src = "//connect.facebook.net/en_US/sdk.js";
						fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));
					
					$(document).ready(function(){
						$('#share_button').click(function(e){
							e.preventDefault();
							FB.ui({
								  method: 'share',
								  href: 'http://localhost:8080<c:url value="/getAction?command=shareAnnotation&annotationId=${annotation.annotationId}" />',
								}, function(response){});
						});
					});
				</script>
				<p><img src="images/share_button.png" id="share_button" /></p>
				<p class="small italic">ID: <c:out value="${annotation.annotationId}" /> from document "<c:out value="${annotation.docId}" />" at position:<c:out value="${annotation.startPosition}" />, <c:out value="${annotation.endPosition}" />.</p>
				</div>
			</c:if>
		</div>
		<footer>
			<div id="footer-content">
				<p>&copy;2015 John Le (s3401071) and Petrina Collingwood
					(s3186652). All rights reserved.</p>
			</div>
		</footer>
	</div>
</body>
</html>