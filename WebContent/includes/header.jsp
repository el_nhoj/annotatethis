<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title>Annotate This! - A document annotation and social reading web application service.</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link href='https://fonts.googleapis.com/css?family=Lora' rel='stylesheet' type='text/css'>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script type="text/javascript" src="scripts/jquery.qtip.min.js"></script>
</head>
<body>
	<div id="wrapper">
		<header>
			<div id="header-content">
				<p><img src="images/banner.png" alt="Logo" /></p>
			</div>
		</header>
		<nav>
			<div id="nav-content">
				<ul>
					<li><a href="getAction?command=userControlPanel">User CP</a></li>
					<li><a href="getAction?command=browseDocuments">Browse Documents</a></li>
					<core:choose>
						<core:when test="${sessionScope.userID != null}">
							<li><a href="getAction?command=logout">Logout</a></li>
						</core:when>
						<core:otherwise>
							<li><a href="/IIDE-Assignment-02">Login</a></li>
						</core:otherwise>
					</core:choose>
				</ul>
			</div>
		</nav>
		<div id="main">