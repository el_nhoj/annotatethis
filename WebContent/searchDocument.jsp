<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ include file="/includes/header.jsp"%>
<script src="scripts/annotation.js" type="text/javascript"></script>
<h1>${filename}</h1>
<form action="getAction" method="POST" name="searchForm">
	<label class="bold">Keyword search (any of these words): </label> <input
		type="text" id="keywords" name="keywords" size="30" />
	<button type="submit" id="searchBtn" name="command" value="viewResults"
		onclick="">Search</button>
</form>
<p class="error">${error}</p>
<p>
	<a href="getAction?command=browseDocuments" class="viewButton">Return
		to Browsing</a>
</p>
<c:forEach var="a" items="${annotations}">
	<c:choose>
		<c:when test="${a.userId == user.userId}">
			<div class="annotated blue" id="${a.annotationId}">
				<p class="heading bold">You wrote:</p>
		</c:when>
		<c:otherwise>
			<c:forEach var="u" items="${users}">
				<c:choose>
					<c:when test="${a.userId == u.userId}">
						<div class="annotated red" id="${a.annotationId}">
							<p class="heading bold">
								<c:out value="${u.name}" />
								wrote:
							</p>
					</c:when>
				</c:choose>
			</c:forEach>
		</c:otherwise>
	</c:choose>
	<div class="content">
		<c:out value="${a.annotationContent}" />
		<br />
		<c:if test="${a.userId == user.userId}">
			<a
				href="getAction?command=shareAnnotation&annotationId=${a.annotationId}"
				class="bold">Share This!</a>
		</c:if>
	</div>
	</div>
</c:forEach>
<script type="text/javascript">
	// For size of array(s), call JS highlight function
	$(document).ready(function() {
		var startPos = [];
		var endPos = [];
		var id = [];
		<c:forEach var="a" items="${annotations}">
			startPos.push(<c:out value="${a.startPosition}"/>);
			endPos.push(<c:out value="${a.endPosition}"/>);
			id.push("${a.annotationId}");
		</c:forEach>
		
		$('#searchBtn').click(function() {
			location.reload(true);	
		});
		
		//Get start and end positions from available annotations
		for (i = 0; i < startPos.length; i++) {
			selectAndHighlightRange('document', startPos[i], endPos[i], id[i]);
		}
	});
</script>
<p id="document">${fileContent}</p>

<%@ include file="/includes/footer.html"%>