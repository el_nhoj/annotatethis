<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ include file="/includes/header.jsp"%>
<script src="scripts/annotation.js" type="text/javascript"></script>
<p class="error">${error}</p>
<h1>${filename}</h1>
<p>
	<br />
	<a href="getAction?command=searchDocument" class="viewButton">Search
		Annotations</a>
</p>
<c:forEach var="a" items="${annotations}">
	<c:choose>
		<c:when test="${a.userId == user.userId}">
			<div class="annotated blue" id="${a.annotationId}">
				<p class="heading bold">You wrote:</p>
		</c:when>
		<c:otherwise>
			<c:forEach var="u" items="${users}">
				<c:choose>
					<c:when test="${a.userId == u.userId}">
						<div class="annotated red" id="${a.annotationId}">
							<p class="heading bold">
								<c:out value="${u.name}" />
								wrote:
							</p>
					</c:when>
				</c:choose>
			</c:forEach>
		</c:otherwise>
	</c:choose>
	<div class="content">
		<c:out value="${a.annotationContent}" />
		<br />
		<c:if test="${a.userId == user.userId}">
			<a href="getAction?command=shareAnnotation&annotationId=${a.annotationId}" class="bold">Share
				This!</a>
		</c:if>
	</div>
	</div>
</c:forEach>
<script type="text/javascript">
	// Get start and end positions from available annotations
	var startPos = [];
	var endPos = [];
	var id = [];
	<c:forEach var="a" items="${annotations}">
		startPos.push(<c:out value="${a.startPosition}"/>);
		endPos.push(<c:out value="${a.endPosition}"/>);
		id.push("${a.annotationId}");
	</c:forEach>

	// For size of array(s), call JS highlight function
	$(document).ready(function() {
		for (i = 0; i < startPos.length; i++) {
			selectAndHighlightRange('document', startPos[i], endPos[i], id[i]);
		}
	});
</script>
<div id="document">${fileContent}</div>
<div class="messagepop pop">
	<form method="post" name="annotationForm" action="getAction">
		<h2>Annotate!</h2>
		<p>
			<label for="annotation">Please enter your annotation</label>
			<textarea rows="6" name="annotationContent" id="annotationContent"
				cols="35" maxlength="200"></textarea>
		</p>
		<p>
			<button type="submit" value="createAnnotation" name="command"
				id="createAnnotation">Save</button>
			or <a class="close" href="/">Cancel</a>
		</p>
		<input type="hidden" name="pos1" id="pos1" value="" /> <input
			type="hidden" name="pos2" id="pos2" value="" /> <input type="hidden"
			name="docID" id="docID" value="${filename}" /> <input type="hidden"
			name="userID" id="userID" value="${user.userId}" />
	</form>
</div>
<%@ include file="/includes/footer.html"%>