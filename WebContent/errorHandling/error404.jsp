<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isErrorPage="true" %>
<%@ include file="/includes/header.jsp"%>
<h1>404 File Not Found</h1>
<p>Oops..The page you requested cannot be found.</p>
<%@ include file="/includes/footer.html"%>