<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/includes/header.jsp"%>
<h1>Browse Documents</h1>
<p>Here is the current list of available documents to view:</p>
<p class="error">${error}</p>
<c:if test="${filenames != null}">
	<form action="getAction" method="post" name="listDocsForm">
		<table>
			<tr>
				<th>&nbsp;</th>
				<th>Document Name</th>
			</tr>
			<c:forEach var="filename" items="${filenames}">
				<tr>
					<td><input type="radio" name="filename" value="${filename}" /></td>
					<td><c:out value="${filename}" /></td>
				</tr>
			</c:forEach>
		</table>
		<button type="submit" name="command" value="viewDocument">View</button>
	</form>
</c:if>
<%@ include file="/includes/footer.html"%>