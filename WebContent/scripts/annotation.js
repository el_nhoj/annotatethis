/**
 * JavaScript/jQuery Annotation Functionality
 */
function deselect(e) {
	$('.pop').slideFadeToggle(function() {
		e.removeClass('selected');
	});
}

$(function() {
	$('#document').on('click', function(e) {
		if ($(this).hasClass('selected')) {
			deselect($(this));
		} else {
			var x = e.pageX + 'px';
			var y = e.pageY + 'px';
			var doc = document.getElementById("document");
			var select = getSelectionCharOffsetsWithin(doc);

			var highlight = getSelection().toString();

			// If selected text isn't highlighted, do nothing
			if (!highlight) {

			} else {
				$(this).addClass('selected');
				$('.pop').slideFadeToggle().css({
					"left" : x,
					"top" : y
				});
				$('input[name="pos1"]').val(select.start);
				$('input[name="pos2"]').val(select.end);
			}
		}
		return false;
	});

	$('.close').on('click', function() {
		deselect($('#document'));
		return false;
	});
});

$.fn.slideFadeToggle = function(easing, callback) {
	return this.animate({
		opacity : 'toggle',
		height : 'toggle'
	}, 'fast', easing, callback);
};

function getSelectionCharOffsetsWithin(element) {
	var start = 0, end = 0;
	var sel, range, priorRange;
	if (typeof window.getSelection != "undefined") {
		range = window.getSelection().getRangeAt(0);
		priorRange = range.cloneRange();
		priorRange.selectNodeContents(element);
		priorRange.setEnd(range.startContainer, range.startOffset);
		start = priorRange.toString().length;
		end = start + range.toString().length;
	} else if (typeof document.selection != "undefined"
			&& (sel = document.selection).type != "Control") {
		range = sel.createRange();
		priorRange = document.body.createTextRange();
		priorRange.moveToElementText(element);
		priorRange.setEndPoint("EndToStart", range);
		start = priorRange.text.length;
		end = start + range.text.length;
	}
	return {
		start : start,
		end : end
	};
}

/**
 * JavaScript Highlight Functionality
 */
function getTextNodesIn(node) {
	var textNodes = [];
	if (node.nodeType == 3) {
		textNodes.push(node);
	} else {
		var children = node.childNodes;
		for (var i = 0, len = children.length; i < len; ++i) {
			textNodes.push.apply(textNodes, getTextNodesIn(children[i]));
		}
	}
	return textNodes;
}

function setSelectionRange(el, start, end) {
	if (document.createRange && window.getSelection) {
		var range = document.createRange();
		range.selectNodeContents(el);
		var textNodes = getTextNodesIn(el);
		var foundStart = false;
		var charCount = 0, endCharCount;

		for (var i = 0, textNode; textNode = textNodes[i++];) {
			endCharCount = charCount + textNode.length;
			if (!foundStart
					&& start >= charCount
					&& (start < endCharCount || (start == endCharCount && i <= textNodes.length))) {
				range.setStart(textNode, start - charCount);
				foundStart = true;
			}
			if (foundStart && end <= endCharCount) {
				range.setEnd(textNode, end - charCount);
				break;
			}
			charCount = endCharCount;
		}

		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);
	} else if (document.selection && document.body.createTextRange) {
		var textRange = document.body.createTextRange();
		textRange.moveToElementText(el);
		textRange.collapse(true);
		textRange.moveEnd("character", end);
		textRange.moveStart("character", start);
		textRange.select();
	}
}

function insertHtmlBeforeSelection(html) {
	var sel, range, node;
	if (window.getSelection) {
		sel = window.getSelection();
		if (sel.getRangeAt && sel.rangeCount) {
			range = window.getSelection().getRangeAt(0);
			range.collapse(true);
			var el = document.createElement("div");
			el.innerHTML = html;
			var frag = document.createDocumentFragment(), node, lastNode;
			while ((node = el.firstChild)) {
				lastNode = frag.appendChild(node);
			}
			range.insertNode(frag);
		}
	} else if (document.selection && document.selection.createRange) {
		range = document.selection.createRange();
		range.collapse(true);
		range.pasteHTML(html);
	}
}

function makeEditableAndHighlight(colour, annotationId) {
	sel = window.getSelection();
	if (sel.rangeCount && sel.getRangeAt) {
		range = sel.getRangeAt(0);
	}
	document.designMode = "on";
	if (range) {
		sel.removeAllRanges();
		sel.addRange(range);
	}
	// Use HiliteColor since some browsers apply BackColor to the whole block
	if (!document.execCommand("HiliteColor", false, colour)) {
		document.execCommand("BackColor", false, colour);
	}
	document.designMode = "off";
	insertHtmlBeforeSelection("<button class='annotationBtn "  + annotationId + "' onclick=\"showDiv('"
			+ annotationId + "')\" ></button>");
}

function highlight(colour, annotationId) {
	var range, sel;
	if (window.getSelection) {
		// IE9 and non-IE
		try {
			if (!document.execCommand("BackColor", false, colour)) {
				makeEditableAndHighlight(colour, annotationId);
			}
		} catch (ex) {
			makeEditableAndHighlight(colour, annotationId)
		}
	} else if (document.selection && document.selection.createRange) {
		// IE <= 8 case
		range = document.selection.createRange();
		range.execCommand("BackColor", false, colour);
	}
}

function selectAndHighlightRange(id, start, end, annotationId) {
	setSelectionRange(document.getElementById(id), start, end);
	highlight("yellow", annotationId);
}

function showDiv(id) {
	$('.annotationBtn').click(function() {
		var btn = $(this);
		
		// If this button has a class matching parameter 'id', set Div below button
		if(btn.hasClass("" + id +"")){
			$("#" + id).css({
				top: btn.offset().top + btn.outerHeight() + 10,
		        left: btn.offset().left
			}).toggle();
		}
	});
}

// function showDiv(id) {
// var element = document.getElementById(id);
// if (element.style.display == "none") {
// element.style.display = "inline-block";
// } else {
// element.style.display = "none";
// }
// }
