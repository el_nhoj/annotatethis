## READ ME ##

# What is it? #
'Annotate This' is a simple social reading and annotation web application written in Java/JSP and uses a SOAP-based web service for database interactions.

# Documents Path #
Please ensure that a valid absolute path to the web project's '/WebContent/documents' directory is set for the following com.rmit.commands classes:

* BrowseDocumentsCommand
* CreateAnnotationCommand
* SearchDocumentCommand
* ViewDocumentCommand
* ViewResultsCommand

# Annotations Path #
Please ensure that a valid absolute path to the web project's '/annotations/' directory is set for the following classes: 

* CreateAnnotationCommand (com.rmit.commands package)
* LuceneSearch (lucene package)

# Contributors #
* John Le
* Petrina Collingwood